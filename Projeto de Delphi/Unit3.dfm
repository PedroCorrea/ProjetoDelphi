object cadProd: TcadProd
  Left = 0
  Top = 0
  Caption = 'Cadastro de Produto'
  ClientHeight = 230
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowMenu = principal.CadastrodeProdutos1
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 36
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Label3: TLabel
    Left = 40
    Top = 80
    Width = 21
    Height = 13
    Caption = 'Cor:'
  end
  object Label4: TLabel
    Left = 40
    Top = 123
    Width = 60
    Height = 13
    Caption = 'Quantidade:'
  end
  object DBEdit1: TDBEdit
    Left = 77
    Top = 33
    Width = 204
    Height = 21
    DataField = 'nome'
    DataSource = DataSource1
    TabOrder = 0
  end
  object DBComboBox1: TDBComboBox
    Left = 77
    Top = 77
    Width = 204
    Height = 21
    DataField = 'cor'
    DataSource = DataSource1
    Items.Strings = (
      'Preto'
      'Cinza'
      'Chumbo'
      'Branco'
      '')
    TabOrder = 1
  end
  object DBRadioGroup1: TDBRadioGroup
    Left = 328
    Top = 25
    Width = 129
    Height = 159
    Caption = 'Tamanho:'
    Ctl3D = True
    DataField = 'tamanho'
    DataSource = DataSource1
    Items.Strings = (
      'PP'
      'P'
      'M'
      'G'
      'GG'
      'XG')
    ParentCtl3D = False
    TabOrder = 2
  end
  object DBEdit2: TDBEdit
    Left = 117
    Top = 120
    Width = 164
    Height = 21
    DataField = 'quantidade'
    DataSource = DataSource1
    TabOrder = 3
  end
  object DBNavigator1: TDBNavigator
    Left = 41
    Top = 159
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 4
  end
  object DataSource1: TDataSource
    DataSet = principal.ADOTable1
    Left = 48
    Top = 208
  end
  object ADOQuery1: TADOQuery
    AutoCalcFields = False
    Connection = principal.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    DataSource = DataSource1
    Parameters = <>
    SQL.Strings = (
      'select * from estoquedelphi')
    Left = 120
    Top = 208
  end
end
