object estoque: Testoque
  Left = 550
  Top = 0
  Caption = 'Estoque'
  ClientHeight = 356
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 130
    Top = 315
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Label2: TLabel
    Left = 24
    Top = 315
    Width = 37
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label3: TLabel
    Left = 320
    Top = 315
    Width = 21
    Height = 13
    Caption = 'Cor:'
  end
  object Label4: TLabel
    Left = 456
    Top = 315
    Width = 21
    Height = 13
    Caption = 'Cor:'
  end
  object Label5: TLabel
    Left = 535
    Top = 315
    Width = 22
    Height = 13
    Caption = 'Qtd:'
  end
  object DBGrid1: TDBGrid
    Left = 24
    Top = 23
    Width = 665
    Height = 273
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Edit1: TEdit
    Left = 167
    Top = 312
    Width = 147
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 614
    Top = 310
    Width = 75
    Height = 25
    Caption = 'Consultar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 67
    Top = 312
    Width = 48
    Height = 21
    TabOrder = 3
  end
  object Edit3: TEdit
    Left = 563
    Top = 312
    Width = 36
    Height = 21
    TabOrder = 4
  end
  object DBComboBox1: TDBComboBox
    Left = 360
    Top = 312
    Width = 90
    Height = 21
    DataField = 'cor'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBComboBox2: TDBComboBox
    Left = 484
    Top = 312
    Width = 45
    Height = 21
    DataField = 'tamanho'
    DataSource = DataSource1
    TabOrder = 6
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = principal.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from estoquedelphi')
    Left = 712
    Top = 32
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 712
    Top = 96
  end
end
